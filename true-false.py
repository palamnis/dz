a = 26
b = 41

# Работа с оператором and
print(a == 18 + 8 and a == 5 * 5 + 1)
print(b > a and 23 < b)
print(b % 2 == 0 and 41 - 25 > a)  # Деление с остатком
print(a < b * 2 / 4 and b == a + 41 - 25)

# Работа с оператором or
print((47 - 36 + 52 / 61) ** 2 > b or a == b + b - 57)
print(a is 27 or b is a)

age = 856
if age < 0 or age > 100:
    print("человек не живой")

ball = 'red'
if ball == "red" or ball == "black": # Случай в казино
    print("вы победили")