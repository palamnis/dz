# Присваивание значений
var_int = 10
var_float = 8.4
var_str = "No"

# Увеличение var_int
big_int = var_int*3.5
print(big_int)

# Уменьшение var_float
var_float = var_float - 1
print(var_float)

# Деление var_int на var_float
var_int = var_int / var_float
print(var_int)

# Деление
big_int = big_int / var_float
print (big_int)

# NoNoYesYesYes
var_str = var_str * 2
var_str = var_str + 'Yes' * 3
print(var_str)