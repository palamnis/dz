# 1
str = '123456789'
print(str[0])
print(str[8])
print(str[2:3])
print(str[-3:-2])
assert len(str) == 9

# 2
a = 'abcdefghijklmno'
print(a[0:8])
print(a[5:-6])
print(a[::3])
print(a[::-1])

# 3
a = 'Pasha'
print('My name is {name}'.format(name=a))

# 4
test_string = "Hello world!"
print(test_string.index("w"))
print(test_string.count('l'))
print(test_string.startswith("Hello"))
print(test_string.endswith("qwe"))
